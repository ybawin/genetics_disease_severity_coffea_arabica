# Supplementary scripts "Genetic composition of Arabica coffee explains fungal disease incidence in the crop’s center of origin"

## Description

The Python scripts in this GitLab project were used to filter genotype calls in the pool-GBS and individual-GBS data that were part of the study about the relationhip between disease severity and genetic diversity and structure in Ethiopian Arabica coffee sites. Four scripts are included:

1. **FilterReproducibleVariants_Pools.py** selects reproducible variants among technical replicates in a variant frequency table (pool-seq, biallelic variants).
2. **FilterBetweenSubgenomePolymorphisms_Pools.py** identifies and removes putative polymorphisms between subgenomes in a variant frequency table of pool-seq data. The script can also discard rare variants and calculate the mean expected heterozygosity per pool in the filtered variant frequency table.
3. **FilterVCF_Individuals.py** excludes sites in a VCF file with a low allele count, mean depth, and/or quality score, and genotypes with a low depth and/or quality score. Non-polymorphic sites and non-biallelic sites can additionally be removed.
4. **FilterBetweenSubgenomePolymorphisms_Individuals.py** identifies and removes putative polymorphisms between subgenomes in a VCF file with individual genotype calls.

## Citation

Zewdie *et al*. (in prep.). Genetic composition of Arabica coffee explains fungal disease incidence in the crop’s center of origin.

## Requirements

All python scripts were developed in python3.6.  
Further requirements are listed, tagged per component: <sup>1</sup>FilterReproducibleVariants_Pools.py, <sup>2</sup>FilterBetweenSubgenomePolymorphisms_Pools.py, <sup>3</sup>FilterVCF_Individuals.py, and <sup>4</sup>FilterBetweenSubgenomePolymorphisms_Individuals.py.

os<sup>1,2,3,4</sup>, sys<sup>1,2,3,4</sup>, argparse<sup>1,2,3,4</sup>, datetime<sup>1,2,3,4</sup>, numpy<sup>1,2,3</sup>, math<sup>1,2</sup>, matplotlib<sup>1,2,3</sup>, collections<sup>3</sup>.

## Building and installing

### 1. FilterReproducibleVariants_Pools.py

The script FilterReproducibleVariants_Pools.py selects reproducible variants among technical replicates in a variant frequency table (pool-seq, biallelic variants). The variant frequency table consists of at least three columns that include the contig/scaffold number of the variant (first column), the contig/scaffold coordinate of the variant (second column), and the variant frequencies (third column and onwards). The header of the columns containing the variant frequencies of a pool must include the corresponding pool name. Columns containing the variant frequencies of technical replicates from the same pool must be grouped together in the variant frequency table.

A reproducible variant is a variant with a frequency between 0 and 1 in all technical replicates. If data is missing in one or more replicates, the data in the other replicate(s) is set missing as well. If all technical replicates have data but the variant frequency in one or more replicates equals 0 or 1 (only one variant was identified), the variant frequencies in the other replicates is set to 0 or 1 as well, respectively. The original variant frequencies are printed to the output file if both variants were identified in each replicate.

The filtered variant frequency table can be converted into a table with only one value for each variant position per pool (*i.e.* the mean variant frequency of all technical replicates) using the --mean_frequency option. In addition, the standard deviation of the mean variant frequency is calculated and printed to a supplementary output file. All tables are stored as tab-delimited text files in the user-defined output directory. The variant frequency distribution of each pool/replicate can also be visualised as a histogram in pdf or png format.

#### Usage

`python3 FilterReproducibleVariants_Pools.py -f FILE [-i INPUT_DIRECTORY] [-n NUMBER_OF_REPLICATES] [--mean_frequency] [-o OUTPUT_DIRECTORY] [--plot] [--plot_type {pdf, png}]`

**Mandatory argument**

    -f, --file  STR  Input variant frequency table.

**Input data options**

	-i, --input_directory      STR	  Input directory [current directory].
	-n, --number_of_replicates INT    Number of replicates of each pool in the variant frequency table [2].
	
**Analysis options**

    --mean_frequency	  Calculate the mean variant frequency for each pool based on the frequencies of technical replicates [mean variant frequencies not calculated].

**Output data options**

    -o, --output_directory  STR   Output directory [current directory].
    --plot                        Plot variant frequency distributions [no plots].
    --plot_type {pdf, png}        File format of plots [pdf].

  
### 2. FilterBetweenSubgenomePolymorphisms_Pools.py

The script FilterBetweenSubgenomePolymorphisms_Pools.py identifies and removes putative polymorphisms between subgenomes in a variant frequency table and a VCF file of pool-seq data. The variant frequency table consists of at least three columns that include the contig/scaffold number of the variant (first column), the contig/scaffold coordinate of the variant (second column), and the variant frequencies (third column and onwards). The header of the columns containing the variant frequencies of a pool must include the corresponding pool name. The VCF file must contain the first (contig/scaffold), second (variant coordinate), fourth (reference nucleotide), and fifth (alternative nucleotide) column. The other columns will be ignored and can be left blanc.

Between-subgenome polymorphisms are characterised by a low variation in variant frequencies among pools. The script expresses the amount of variation among pools as the coefficient of variation (CV) or as F<sub>ST</sub>. Variants with a CV or F<sub>ST</sub> value lower than the user-defined minimum (via the option --min_var) are excluded from the VCF file. CV and F<sub>ST</sub> are calculated as follows:

*CV = SD / m* (Lovie, 2005)

*F<sub>ST</sub> = (H<sub>T</sub> - H<sub>S</sub>) / H<sub>T</sub>* (Nei &  Chesser, 1983)

With *SD* the standard deviation and *m* the mean. *H<sub>T</sub>* (the expected heterozygosity under Hardy-Weinberg equilibrium in all pools) and H<sub>S</sub> (the expected heterozygosity under Hardy-Weinberg equilibrium in each pool) are defined as follows:

*H<sub>S</sub> = 1 - (1 / K) x ∑(k=1)^K [p<sub>k</sub><sup>2</sup> + (1-p<sub>k</sub>)<sup>2</sup> ]*

*H<sub>T</sub>=1-[M<sup>2</sup>+(1-M)<sup>2</sup> ]*

With *K* the number of pools, *p<sub>k</sub>* the variant frequency of pool *k*, and *M* the mean variant frequency. The CV or F<sub>ST</sub> values of all variants are stored in a separate tab-delimited text file in the user-defined output directory.

Alternatively, polymorphisms between subgenomes are often characterised by a high number of pools with a frequency around 0.5. Hence, removing all variants with a frequency around 0.5 may also exclude between-subgenome polymorphisms in the variant frequency table. This filtering procedure is used if the --medium_filter option is turned on. The options --med_min and --med_max define the lower (*e.g.* 0.4) and upper (*e.g.* 0.6) frequency limits, respectively. The minimum number of pools that must pass this filter is specified with the -w (weight) option. So the --medium_filter option discards all variants in the variant frequency table with a frequency between --med_min and --med_max in less than -w pools.

Variants with a frequency lower or higher than the user-defined minimum (--min) and maximum (--max) in less than -w pools can also be removed from the frequency table. This option can for instance be used to remove rare variants (*e.g.* with a frequency lower than 0.05 or higher than 0.95 in at least -w pools). After all filtering steps, the mean expected heterozygosity can be calculated for each pool. The expected heterozygosity is calculated as follows:

*He = 2 x freq x (1 - freq)*

with *freq* the variant frequency. The expected heterozygosity values of every variant in a pool are printed to an output table. The mean expected heterozygosity of a pool, which is calculated as the mean of the expected heterozygosity value of all variants in the pool, is provided at the bottom of each column. The standard deviation of the mean expected heterozygosity is also added to each column. All tables are stored as tab-delimited text files in the user-defined output directory. The mean expected heterozygosity can be plotted as a bar chart in pdf or png format. The variant frequency distribution of each pool can also be visualised as a histogram in pdf or png format.

#### Usage

`python3 FilterBetweenSubgenomePolymorphisms_Pools.py --RAF_file RAF_FILE --VCF_file VCF_FILE [-i INPUT_DIRECTORY] [-c COMPLETENESS] [-var {None, CV, Fst}] [--min_var MIN_VAR] [--filter] [-min MINIMUM] [-max MAXIMUM] [--medium_filter] [-med_min MED_MIN] [-med_max MED_MAX] [-w WEIGHT] [--expected_heterozygosity] [-o OUTPUT_DIRECTORY] [--plot] [--plot_type {pdf, png}]`

**Mandatory arguments**

    --RAF_file  STR  Input variant frequency table.
    --VCF_file  STR  Input VCF file.

**Input data options**

	-i, --input_directory  STR	  Input directory [current directory].
	
**Analysis options**

    -c, --completeness          FLOAT  Minimum proportion of data completeness in a variant [0].
    -var, --variation {None, CV, Fst}  Criterion for variant filtering [no criterion specified].
    --min_var                   FLOAT  Minimum value for filtering on one of the variation criteria [0].
    --filter                           Filter the variant frequency table [variant frequency table not filtered].
    -min, --minimum             FLOAT  Minimum variant frequency [0].
    -max, --maximum             FLOAT  Maximum variant frequency [1].
    --medium_filter                    Filter the variant frequency table to exclude variants with a frequency higher than the predefined --med_min and smaller than the predefined --med_max [variant frequency table not medium_filtered].
    -w, --weight                INT    Minimum number of individuals with a variant frequency that corresponds to the predefined minimum and maximum parameters [0].
    --expected_heterozygosity          Calculate the expected heterozygosity for biallelic variants (He = 2 * freq * (1 - freq)) [expected heterozygosity not calculated].

**Output data options**

    -o, --output_directory  STR   Output directory [current directory].
    --plot                        Plot variant frequency distributions [no plots].
    --plot_type {pdf, png}        File format of plots [pdf].

### 3. FilterVCF_Individuals.py

The script FilterVCF_Individuals.py excludes sites in a VCF file (VCF v4.2) with a low allele count, mean depth, and/or quality score, and genotypes with a low depth and/or quality score. Non-polymorphic sites and non-biallelic sites can additionally be removed. The script accepts diploid or polyploid genotype calls. The filtered data are stored in a VCF file in the same directory as the original VCF file.

#### Usage

`python3 FilterVCF_Individuals.py --vcf VCF [--min_meanDP MIN_MEANDP] [--minMAC MINMAC] [--minQ MINQ] [--polymorphic] [--biallelic] [--minGQ MINGQ] [--minDP MINDP] [--ploidy]`

**Mandatory argument**

    --vcf  STR  Path to the vcf file.

**Input data options**
    
    --ploidy  INT  Ploidy level of genotype calls in the VCF file.

**Filter options for site characteristics**

	--min_meanDP  FLOAT   Minimum depth of a site averaged over all samples [0].
	--minMAC      FLOAT   Minimum minor allele count of a site [0].
	--minQ        FLOAT   Minimum quality of a site [0].
	--polymorphic         Remove non-polymorphic sites [non-polymorphic sites not removed].
	--biallelic           Remove non-biallelic sites [non-polymorphic sites not removed].
	
**Filter options for genotype characteristics**

	--minGQ       FLOAT   Minimum quality score of a genotype [0].
	--minDP       INT     Minimum depth of a genotype [0].
  
### 4. FilterBetweenSubgenomePolymorphisms_Individuals.py

The script FilterBetweenSubgenomePolymorphisms_Individuals.py identifies and removes putative polymorphisms between subgenomes in a VCF file (VCF v4.2) with individual genotype calls. Between-subgenome polymorphisms in data of individuals are often characterised by a high number of individuals with a heterozygous genotype call. The script allows users to filter the original VCF file for variants with heterozygous genotype calls in less than a user-defined proportion of individuals (via the option -mh) in a population. All individuals of the same populations must be grouped in the original VCF file and the column number of the last individual of each population in the VCF file must be parsed to the script as a dash-separated list via the --populations option. The script also allows the user to discard variants with a frequency lower than the user-defined minimum (via the option -mf) in all populations. After all filtering steps, the mean expected heterozygosity can be calculated for each individual. The expected heterozygosity is calculated as follows:

*He = 2\timesfreq\times(1 - freq)*

with *freq* the variant frequency of a genotype call. The mean expected heterozygosity of an individual is calculated as the mean of the expected heterozygosity value of all variants in the individual. All tables are stored as tab-delimited text files in the user-defined output directory. The mean expected heterozygosity can also be visualised as a bar chart in pdf or png format.

#### Usage

`python3 FilterBetweenSubgenomePolymorphisms_Individuals.py --VCF_file VCF_FILE [-i INPUT_DIRECTORY] [--populations POPULATIONS] [-c COMPLETENESS] [-mh MAXIMUM_PROPORTION_HETEROZYGOUS] [-mf MINIMUM_FREQUENCY] [--expected_heterozygosity] [-o OUTPUT_DIRECTORY] [--plot] [--plot_type {pdf, png}]`

**Mandatory argument**

    --VCF_file  STR  Input VCF file.

**Input data options**

	-i, --input_directory  STR    Input directory [current directory].
	--populations          STR    Dash-separated list with the column numbers in the snapepooled output file (starting with 1) of the last individual of each population (e.g. 10-20 in case of two populations with ten individuals each) [no populations specified].
	
**Analysis options**

    -c, --completeness                     FLOAT  Minimum proportion of data completeness in a variant [0].
    -mh, --maximum_proportion_heterozygous FLOAT  Maximum proportion of heterozygous samples [0].
    -mf, --minimum_frequency               FLOAT  Minimum frequency of a variant [0].
    --expected_heterozygosity                     Calculate the expected heterozygosity for biallelic variants (He = 2 * freq * (1 - freq)) [expected heterozygosity not calculated].
    
**Output data options**

    -o, --output_directory  STR   Output directory [current directory].
    --plot                        Plot variant frequency distributions [no plots].
    --plot_type {pdf, png}        File format of plots [pdf].

## References

Lovie, P. (2005). Coefficient of Variation. In B. S. Everitt & D. C. Howell (Eds.), *Encyclopedia of Statistics in Behavioral Science* (1st ed., Vol. 1, pp. 317–318). Chichester: John Wiley & Sons. https://doi.org/10.4135/9781412961288.n56

Nei, M., & Chesser, R. K. (1983). Estimation of fixation indices and gene diversities. *Annals of Human Genetics, 47*(3), 253–259. https://doi.org/10.1111/j.1469-1809.1983.tb00993.x