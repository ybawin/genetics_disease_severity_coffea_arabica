#!usr/bin/python3

#===============================================================================
# FilterBetweenSubgenomePolymorphisms_Pools
#===============================================================================
#Yves BAWIN August 2018
#Python script for the identification and removal of polymorphisms
#between subgenomes in a variant frequency table containing genotype calls of pools.

#===============================================================================
# Import modules
#===============================================================================

import os, argparse, sys, math
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt

#===============================================================================
# Parse arguments
#===============================================================================

#Create an ArgumentParser object
parser = argparse.ArgumentParser(description = 'Filter the variant frequency table to remove polymorphisms between subgenomes and calculate the expected heterozygosity.')

#Mandatory argument
parser.add_argument('--RAF_file',
                    type = str,
                    help = 'Input variant frequency table.')
parser.add_argument('--VCF_file',
                    type = str,
                    help = 'Input VCF file.')

#Optional arguments
'''
Input data options.
'''
parser.add_argument('-i', '--input_directory',
                    type = str,
                    default = '.',
                    help = 'Input directory (default = current directory).')

'''
Analysis options.
'''
parser.add_argument('-c', '--completeness',
                    type = float,
                    default = 0,
                    help = 'Minimum proportion of data completeness in a variant (default = 0).')
parser.add_argument('-var', '--variation',
                    choices = [None, 'CV', 'Fst'],
                    type = str, 
                    default = None,
                    help = 'Criterion for variant filtering (default = no criterion specified).')
parser.add_argument('--min_var', 
                    type = float,
                    default = 0,
                    help = 'Minimum value for filtering on one of the variation criteria (default = 0).')
parser.add_argument('--filter',
                    dest = 'filter',
                    action = 'store_true',
                    help = 'Filter the variant frequency table (default = variant frequency table not filtered).')
parser.add_argument('-min', '--minimum',
                    type = float,
                    default = 0,
                    help = 'Minimum variant frequency (default = 0).')
parser.add_argument('-max', '--maximum',
                    type = float,
                    default = 1,
                    help = 'Maximum variant frequency (default = 1).')
parser.add_argument('--medium_filter',
                    dest = 'medium_filter',
                    action = 'store_true',
                    help = 'Filter the variant frequency table to exclude variants with a frequency higher than the predefined --medium_minimum and smaller than the predefined --medium_maximum (default = no filtering).')
parser.add_argument('-med_min', '--medium_minimum',
                    type = float,
                    default = 0.5,
                    help = 'Medium minimum variant frequency (default = 0.5)')
parser.add_argument('-med_max', '--medium_maximum',
                    type = float,
                    default = 0.5,
                    help = 'Medium maximum variant frequency (default = 0.5).')
parser.add_argument('-w', '--weight',
                    type = int,
                    default = 0,
                    help = 'Minimum number of individuals with a variant frequency that corresponds to the predefined minimum and maximum parameters (default = 0).')
parser.add_argument('--expected_heterozygosity',
                    dest = 'He',
                    action = 'store_true',
                    help = 'Calculate the expected heterozygosity for biallelic variants (He = 2 * freq * (1 - freq)) (default = expected heterozygosity not calculated).')

'''
Output data options.
'''
parser.add_argument('-o', '--output_directory',
                    type = str,
                    default = '.',
                    help = 'Output directory (default = current directory).')
parser.add_argument('--plot',
                    dest = 'plot',
                    action = 'store_true',
                    help = 'Plot variant frequency distributions (default = no plots).')
parser.add_argument('--plot_type',
                    choices = ['pdf', 'png'],
                    default = 'pdf',
                    help = 'File format of plots (default = pdf).')

#Parse arguments to a dictionary
args = vars(parser.parse_args())

#===============================================================================
# Functions
#===============================================================================
def print_date ():
    '''
    Print the current date and time to stderr.
    '''
    sys.stderr.write('-------------------\n')
    sys.stderr.write('{}\n'.format(datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
    sys.stderr.write('-------------------\n\n')
    return


def Filter_var(RAF_file = args['RAF_file'], VCF_file = args['VCF_file'], in_dir = args['input_directory'], out_dir = args['output_directory'], var = args['variation'], min_var = args['min_var']):
    '''
    Filter the SNP file to remove putative between-subgenome polymorphisms based on the coefficient of variation or Fst value of each site.
    Two output file is created: *_filtered.txt
                                *_filtered.vcf
    '''
    #Extract the file names.
    RAF_name = os.path.splitext(RAF_file)[0]
    VCF_name = os.path.splitext(VCF_file)[0]
    
    #Open the allele table and the read depth table.
    RAF_file = open('{}/{}'.format(in_dir, RAF_file))
    VCF_file = open('{}/{}'.format(in_dir, VCF_file))
    
    #Create the output files for the filtered data.
    new_RAF_file = open('{}/{}_filtered_{}{}.txt'.format(out_dir, RAF_name, var, min_var),'w+')
    new_VCF_file = open('{}/{}_filtered_{}{}.vcf'.format(out_dir, VCF_name, var, min_var),'w+')
    var_file = open('{}/{}_{}.txt'.format(out_dir, RAF_name, var), 'w+')
    
    #Print a header in the CV output file.
    if var == 'CV':
        print('Chromosome\tPosition\tMean\tdev\tStandard_deviation\tCV', file = var_file)
    else:
        print('Chromosome\tPosition\tHs\tHt\tFst', file = var_file)
        
    #Extract the headers from the input file and print them to the output files.
    header = RAF_file.readline()
    print(header, end = '', file = new_RAF_file)
    print(VCF_file.readline(), end = '', file = new_VCF_file)
    header = VCF_file.readline()
    print(header, end = '', file = new_VCF_file)
    
    #Iterate over the SNP frequencies in one SNP line and delete missing values.
    for RAF_line, VCF_line in zip(RAF_file, VCF_file):
        RAF_line = RAF_line.rstrip().split('\t')
        VCF_line = VCF_line.rstrip().split('\t')
        assert RAF_line[0] == VCF_line[0] and RAF_line[1] == VCF_line[1], 'The SNPs in the allele frequency file and the VCF file are not ordered in the same way.'
        values = [float(x) for x in RAF_line[2:] if x != '']
        
        if var == 'CV':
            #Calculate the mean.
            m = np.mean(values)
            
            #Calculate the standard deviation.
            dev = sum([(x - m)**2 for x in values])
            s = math.sqrt(dev/(len(values) - 1))
            
            #Calculate the coefficient of variation.
            v = s / m
            if m == 0 or s == 0 or len(values) == 1:
                v = 0
            print('{}\t{}\t{}\t{}\t{}'.format(RAF_line[0], RAF_line[1], m, s, v), file = var_file)
            
        elif var == 'Fst': #Definition of Fst derived from Nei & Chesser (1983)
            #Calculate the mean heterozygosity of a set of subpopulations Hs.
            freq = sum([x**2 + (1 - x)**2 for x in values])
            Hs = 1 - (1/len(values)) * freq
            
            #Calculate the total heterozygosity Ht.
            m = np.mean(values)
            Ht = 1 - (m**2 + (1 - m)**2)
            
            #Calculate Fst.
            v = (Ht - Hs)/Ht
            if Ht == 0:
                v = 0
            print('{}\t{}\t{}\t{}\t{}'.format(RAF_line[0], RAF_line[1], Hs, Ht, v), file = var_file)
            
        #Keep the variant in the frequency table if the value exceeds the minimum.
        if v >= min_var:
            print('\t'.join(value for value in RAF_line), file = new_RAF_file)
            print('\t'.join(value for value in VCF_line), file = new_VCF_file)
            
    return


def Filter_freq(RAF_file = args['RAF_file'], VCF_file = args['VCF_file'], in_dir = args['input_directory'], out_dir = args['output_directory'], var = args['variation'], min_var = args['min_var'], min = args['minimum'], max = args['maximum'], medium_filter = args['medium_filter'], med_min = args['medium_minimum'], med_max = args['medium_maximum'], weight = args['weight'], c = args['completeness']):
    '''
    Filter the SNP file to delete SNPs that are homozygous (within the predefined minimum and maximum range) in all samples.
    Two output file is created: *_filtered_(min_max)_(med_min_med_max)_(w)_(c).txt
                                *_filtered_(min_max)_(med_min_med_max)_(w)_(c).vcf
    '''
    #Extract the file names.
    RAF_name = os.path.splitext(RAF_file)[0]
    VCF_name = os.path.splitext(VCF_file)[0]
    
    #Open the variant frequency table and the VCF file.
    if var is not None:
        RAF_file = open('{}/{}_filtered_{}{}.txt'.format(out_dir, RAF_name, var, min_var))
        VCF_file = open('{}/{}_filtered_{}{}.vcf'.format(out_dir, VCF_name, var, min_var))
    else:
        RAF_file = open('{}/{}'.format(in_dir, RAF_file))
        VCF_file = open('{}/{}'.format(in_dir, VCF_file))
    
    #Create the output files for the filtered data.
    if var is not None:
        new_RAF_file = open('{}/{}_filtered_{}{}_min{}_max{}{}{}{}.txt'.format(out_dir, RAF_name, var, min_var, min, max, '_med_min{}_med_max{}'.format(med_min, med_max) if medium_filter else '', '_w{}'.format(weight) if weight != 0 else '', '_c{}'.format(c) if c != 0 else ''),'w+')
        new_VCF_file = open('{}/{}_filtered_{}{}_min{}_max{}{}{}{}.vcf'.format(out_dir, VCF_name, var, min_var, min, max, '_med_min{}_med_max{}'.format(med_min, med_max) if medium_filter else '', '_w{}'.format(weight) if weight != 0 else '', '_c{}'.format(c) if c != 0 else ''),'w+')
    else:
        new_RAF_file = open('{}/{}_filtered_min{}_max{}{}{}{}.txt'.format(out_dir, RAF_name, min, max, '_med_min{}_med_max{}'.format(med_min, med_max) if medium_filter else '', '_w{}'.format(weight) if weight != 0 else '', '_c{}'.format(c) if c != 0 else ''),'w+')
        new_VCF_file = open('{}/{}_filtered_min{}_max{}{}{}{}.vcf'.format(out_dir, VCF_name, min, max, '_med_min{}_med_max{}'.format(med_min, med_max) if medium_filter else '', '_w{}'.format(weight) if weight != 0 else '', '_c{}'.format(c) if c != 0 else ''),'w+')
    
    #Extract the headers from the input files and print them to the output files.
    header = RAF_file.readline()
    print(header, end = '', file = new_RAF_file)
    print(VCF_file.readline(), end = '', file = new_VCF_file)
    header = VCF_file.readline()
    print(header, end = '', file = new_VCF_file)
    
    #Iterate over the variant lines in the input file.
    for RAF_line, VCF_line in zip(RAF_file, VCF_file):
        RAF_line = RAF_line.rstrip().split('\t')
        VCF_line = VCF_line.rstrip().split('\t')
        assert RAF_line[0] == VCF_line[0] and RAF_line[1] == VCF_line[1], 'The SNPs in the allele frequency file and the VCF file are not ordered in the same way.'
        
        hom_count = 0
        het_count = 0
        
        #If the frequency of a variant is higher than the minimum and lower than the maximum and/or it is lower than the medium minimum or higher than the medium maximum, add 1 to the count of frequencies that fit the criteria.
        #Missing values are considered as values that do not fit the criteria.
        for freq in RAF_line[2:]:
            if freq != '' and min < float(freq) < max:
                hom_count += 1
                if medium_filter and ((float(freq) < med_min) or (med_max < float(freq))):
                    het_count += 1
                    
        #Create a list with no missing values
        line_wo_NA = list(filter(None, RAF_line))
        
        #The variant is printed to the output files if the number of frequencies that fit the criteria is larger than the predefined minima.
        if (hom_count >= weight and (not medium_filter)) and (len(line_wo_NA) - 2) / (len(RAF_line) - 2) >= c:
            print('\t'.join(value for value in RAF_line), file = new_RAF_file)
            print('\t'.join(value for value in VCF_line), file = new_VCF_file)
        elif (hom_count >= weight and het_count >= weight) and (len(line_wo_NA) - 2) / (len(RAF_line) - 2) >= c:
            print('\t'.join(value for value in RAF_line), file = new_RAF_file)
            print('\t'.join(value for value in VCF_line), file = new_VCF_file)
    return


def Heterozygosity(file = args['RAF_file'], in_dir = args['input_directory'], out_dir = args['output_directory'], filter = args['filter'], var = args['variation'], min_var = args['min_var'], medium_filter = args['medium_filter'], plot = args['plot'], min = args['minimum'], max = args['maximum'], med_min = args['medium_minimum'], med_max = args['medium_maximum'], weight = args['weight'], plot_type = args['plot_type'], c = args['completeness']):
    '''
    Calculate the expected heterozygosity for each variant.
    Two output files are created: *_mean_variant_freq_He.txt (a table with all expected heterozygosity values)
                                  *_mean_expected_heterozygosity.pdf/.png
    '''
    #Extract the name of the input file.
    name = os.path.splitext(file)[0]
    
    #Open the input file and create a new output file.
    file = open('{}/{}{}{}{}{}{}.txt'.format(out_dir if filter or medium_filter or var is not None else in_dir, name, '_filtered{}{}'.format('_{}'.format(var) if var is not None else '', '{}'.format(min_var) if var is not None else '') if filter or medium_filter or var is not None else '', '_min{}_max{}'.format(min, max) if filter else '', '_med_min{}_med_max{}'.format(med_min, med_max) if medium_filter else '', '_w{}'.format(weight) if weight != 0 else '', '_c{}'.format(c) if c != 0 else ''))
    new_file = open('{}/{}{}{}{}{}{}_He.txt'.format(out_dir, name, '_filtered{}{}'.format('_{}'.format(var) if var is not None else '', '{}'.format(min_var) if var is not None else '') if filter or var is not None or medium_filter else '', '_min{}_max{}'.format(min, max) if filter else '', '_med_min{}_med_max{}'.format(med_min, med_max) if medium_filter else '', '_w{}'.format(weight) if weight != 0 else '', '_c{}'.format(c) if c != 0 else ''), 'w+')
    
    #Extract the header from the input file.
    header = file.readline()
    
    #Print the header to the output file.
    print(header, end = '', file = new_file)
    
    #Create three dictionaries with the sample names as keys (one for the He's, another for the sum of the present data, and a third one for the standard errors).
    header = header.rstrip().split('\t')[2:]
    
    He = dict()
    present = dict()
    SD_dict = dict()
    SE_dict = dict()
    
    #Add the sample names as keys to each created dictionary
    for name in header:
        He[name] = list()
        present[name] = float()
        SD_dict[name] = float()
        SE_dict[name] = float()
    
    #Iterate over the SNP lines.
    for line in file:
        line = line.rstrip().split('\t')
        #Print the contig number and the position to the output file.
        print('{}\t{}\t'.format(line[0], line[1]), end = '', file = new_file)
        
        He_list = list()
        
        #Iterate over the frequencies.
        for i, freq in enumerate(line[2:]):
            #If the data are present, calculate the expected heterozygosity (He) as 2 * AF * (1 - AF). He is set to '' if the data are missing.
            #Sum this value to the other values calculated for this sample.
            #Add 1 to the corresponding value in the dictionary that counts the amount of data present for each sample.
            if freq != '':
                value = 2 * float(freq) * (1 - float(freq))
                He[header[i]].append(value)
                present[header[i]] += 1
            else:
                value = ''
                
            #Append He to a dictionary with the He's combined per sample and a list with all He's.
            He_list.append(value)
        #Print all He's to the output file.
        print('\t'.join(str(value) for value in He_list), file = new_file)
    
    #Print a column name to the row with the average He's
    print('Mean_He\t\t', end = '', file = new_file)
    
    #Calculate the mean He and the standard deviation for each sample
    means = list()
    SEs = list()
    for name in header:
        mean = np.mean(He[name])
        means.append(mean)
        
        #Calculate the Standard deviation.
        SD = 0
        for value in He[name]:
            SD += (float(value) - float(mean))**2
        SD = math.sqrt(float(SD)/(float(present[name]) - 1.0))
        SD_dict[name] = SD
        SE = float(SD)/float(math.sqrt(present[name]))
        SEs.append(SE)
        SE_dict[name] = SE
        
    print('\t'.join(str(mean) for mean in means), file = new_file)
    
    #Print a column name to the row with the standard error on the He.
    print('St_dev_He\t\t', end = '', file = new_file)
    
    #Print the standard deviations
    print('\t'.join(str(SD_dict[name]) for name in header), file = new_file)
    
    #Print the standard error
    print('St_err_He\t\t', end = '', file = new_file)
    print('\t'.join(str(SE_dict[name]) for name in header), file = new_file)
    
    if plot:
        sys.stderr.write('* Plotting the mean expected heterozygosity per population in a barplot ...\n')
        #Create a barplot of the mean expected heterozygosity with standard error.
        r = list(np.arange(1, len(means) + 1))
        
        plt.figure(figsize=(10,10))
        plt.bar(r, means, color = 'g', yerr = SEs)
        plt.title('Mean_expected_Heterozygosity{}\n{}{}{}{}'.format('_filtered_{}'.format(var if var is not None else '') if filter or medium_filter or var is not None else '', '_min{}_max{}'.format(min, max) if filter else '', '_med_min{}_med_max{}'.format(med_min, med_max) if medium_filter else '', '_weight{}'.format(weight) if weight != 0 else '', '_completeness{}'.format(c) if c != 0 else ''))
        plt.xlabel('Population')
        plt.ylabel('Mean expected heterozygosity')
        plt.xticks(np.arange(0,len(means) + 1,5))
        if plot_type == 'pdf':
            plt.savefig('{}/Mean_expected_Heterozygosity{}{}{}{}{}.pdf'.format(out_dir, '_filtered{}{}'.format('_{}'.format(var) if var is not None else '', min_var if var is not None else '') if filter or medium_filter or var is not None else '', '_min{}_max{}'.format(min, max) if filter else '', '_med_min{}_med_max{}'.format(med_min, med_max) if medium_filter else '', '_w{}'.format(weight) if weight != 0 else '', '_c{}'.format(c) if c != 0 else ''), format = 'pdf')
        else:
            plt.savefig('{}/Mean_expected_Heterozygosity{}{}{}{}{}.png'.format(out_dir, '_filtered{}{}'.format('_{}'.format(var) if var is not None else '', min_var if var is not None else '') if filter or medium_filter or var is not None else '', '_min{}_max{}'.format(min, max) if filter else '', '_med_min{}_med_max{}'.format(med_min, med_max) if medium_filter else '', '_w{}'.format(weight) if weight != 0 else '', '_c{}'.format(c) if c != 0 else ''), format = 'png')
        plt.close()
    return

def histogram (file = args['RAF_file'], in_dir = args['input_directory'], out_dir = args['output_directory'], plot_type = args['plot_type'], filter = args['filter'], var = args['variation'], min_var = args['min_var'], min = args['minimum'], max = args['maximum'], medium_filter = args['medium_filter'], med_min = args['medium_minimum'], med_max = args['medium_maximum'], weight = args['weight'], c = args['completeness']):
    '''
    General function to create a histogram for the given values.
    '''
    
    #Extract the file name.
    name = os.path.splitext(file)[0]
    
    #Open the input file.
    file = open('{}/{}{}{}{}{}{}.txt'.format(out_dir if filter or medium_filter or var is not None else in_dir, name, '_filtered{}{}'.format('_{}'.format(var) if var is not None else '', min_var if var is not None else '') if filter or medium_filter or var is not None else '', '_min{}_max{}'.format(min, max) if filter else '', '_med_min{}_med_max{}'.format(med_min, med_max) if medium_filter else '', '_w{}'.format(weight) if weight != 0 else '', '_c{}'.format(c) if c != 0 else ''))
    values = dict()
    
    #Create a histogram.
    header = file.readline().rstrip().split('\t')[2:]
    for name in header:
        values[name] = list()
    
    for line in file:
        line = line.rstrip().split('\t')[2:]
        for i, value in enumerate(line):
            if value != '' and 0 < float(value) < 1:
                values[header[i]].append(float(value))
    
    for name, value in values.items():
        plt.figure(figsize=(10,10))
        plt.hist(value, alpha = 0.7, color = 'darkslategray', bins=(np.linspace(0,1,101)))
        plt.title('{}{}\n{}{}{}{}'.format(name, '_filtered_{}_{}'.format(var if var is not None else '', min_var if var is not None else '') if filter or medium_filter or var is not None else '', '_min{}_max{}'.format(min, max) if filter else '', '_med_min{}_med_max{}'.format(med_min, med_max) if filter else '', '_w{}'.format(weight) if weight != 0 else '', '_c{}'.format(c) if c != 0 else ''))
        plt.xlabel('Variant frequency')
        plt.ylabel('Frequency')
        
        if plot_type == 'pdf':
            plt.savefig('{}/{}{}{}{}{}{}_histogram.pdf'.format(out_dir, name, '_filtered{}{}'.format('_{}'.format(var) if var is not None else '', min_var if var is not None else '') if filter or medium_filter or var is not None else '', '_min{}_max{}'.format(min, max) if filter else '', '_med_min{}_med_max{}'.format(med_min, med_max) if medium_filter else '', '_w{}'.format(weight) if weight != 0 else '', '_c{}'.format(c) if c != 0 else ''), format='pdf')
        else :
            plt.savefig('{}/{}{}{}{}{}{}_histogram.png'.format(out_dir, name, '_filtered{}{}'.format('_{}'.format(var) if var is not None else '', min_var if var is not None else '') if filter or medium_filter or var is not None else '', '_min{}_max{}'.format(min, max) if filter else '', '_med_min{}_med_max{}'.format(med_min, med_max) if medium_filter else '', '_w{}'.format(weight) if weight != 0 else '', '_c{}'.format(c) if c != 0 else ''), format='png')
        plt.close()
    
    return

#===============================================================================
# Script
#===============================================================================

if __name__ == '__main__':

    print_date()
    if args['variation'] is not None:
        sys.stderr.write('* Filtering variants for an {} value not lower than {} ...\n'.format(args['variation'], args['min_var']))
        Filter_var()
        
    if args['filter'] or args['medium_filter']:
        sys.stderr.write('* Filtering variants based on frequency and/or data completeness ...\n')
        Filter_freq()
    
    if args['He']:
        sys.stderr.write('* Calculating the expected heterozygosity for each variant ...\n')
        Heterozygosity()
    
    if args['plot']:
        sys.stderr.write('* Plotting the variant frequencies of each sample in a histogram ...\n')
        histogram()
    
    sys.stderr.write('* Finished!\n\n')
    
    print_date()
