#!usr/bin/python3

#===============================================================================
# FilterBetweenSubgenomePolymorphisms_Individuals
#===============================================================================

#Yves BAWIN March 2020
#Python script for the identification and removal of polymorphisms
#between subgenomes in a vcf file with genotype calls of individuals.

#===============================================================================
# Import modules
#===============================================================================

import os, sys, argparse
from datetime import datetime
from collections import defaultdict
import numpy as np
import matplotlib.pyplot as plt

#===============================================================================
# Parse arguments
#===============================================================================

#Create an ArgumentParser object
parser = argparse.ArgumentParser(description = 'Identify and remove polymorphisms between subgenomes in a vcf file.')

#Mandatory arguments
parser.add_argument('--VCF_file',
                    type = str,
                    help = 'Name of the vcf file.')

#Optional arguments
'''
Input data options.
'''
parser.add_argument('-i', '--input_directory',
                    type = str,
                    default = '.',
                    help = 'Input directory (default = current directory).')
parser.add_argument('--populations',
                    type = str,
                    default = None,
                    help = 'Dash-separated list with the column numbers in the snapepooled output file (starting with 1) of the last individual of each population (e.g. 10-20 in case of two populations with ten individuals each) (default = no populations specified).')

'''
Analysis options.
'''
parser.add_argument('-c', '--completeness',
                    type = float,
                    default = 0,
                    help = 'Minimum proportion of data completeness in a variant (default = 0).')
parser.add_argument('-mh', '--maximum_proportion_heterozygous',
                    type = float,
                    default = 0,
                    help = 'Maximum proportion of heterozygous samples (default = 0).')
parser.add_argument('-mf', '--minimum_frequency',
                    type = float,
                    default = 0,
                    help = 'Minimum frequency of a variant (default = 0).')
parser.add_argument('--expected-heterozygosity',
                    dest = 'He',
                    action = 'store_true',
                    help = 'Calculate the expected heterozygosity for biallelic variants (He = 2 * freq * (1 - freq)) (default = expected heterozygosity not calculated)..')

'''
Output data options.
'''
parser.add_argument('-o', '--output_directory',
                    type = str,
                    default = '.',
                    help = 'Output directory (default = current directory).')
parser.add_argument('--plot',
                    dest = 'plot',
                    action = 'store_true',
                    help = 'Plot variant frequency distributions (default = no plots).')
parser.add_argument('--plot_type',
                    choices = ['pdf', 'png'], 
                    default = 'pdf',
                    help = 'File format of plots (default = pdf).')

#Parse arguments to a dictionary
args = vars(parser.parse_args())

#===============================================================================
# Functions
#===============================================================================
def print_date ():
    '''
    Print the current date and time to stderr.
    '''
    sys.stderr.write('-------------------\n')
    sys.stderr.write('{}\n'.format(datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
    sys.stderr.write('-------------------\n\n')
    return


def filterVCF(vcf = args['VCF_file'], in_dir = args['input_directory'], out_dir = args['output_directory'], pop = args['populations'], max_prop_het = args['maximum_proportion_heterozygous'], c = args['completeness'], min_freq = args['minimum_frequency']):
    '''
    Filter a vcf file to identify and exclude polymorphisms between subgenomes and to exclude variants with a frequency lower than the predefined minimum in every sample.
    One output file is created: *_mh*_mf*_c*.vcf
    '''
    #Extract the name of the vcf file.
    name_vcf = os.path.splitext(vcf)[0]
    
    #Open the vcf file and create a new vcf file.
    vcf = open('{}/{}'.format(in_dir, vcf))
    vcf_new = open('{}/{}_mh{}_mf{}_c{}.vcf'.format(out_dir, name_vcf, max_prop_het, min_freq, c), 'w+')
    
    #Extract the header and print it to the new vcf file.
    header = vcf.readline()
    print(header, end = '', file = vcf_new)
    
    #Extract the genotypes of the samples that must be retained from the original vcf file.
    if pop:
        populations = pop.split('-')
    else:
        populations = len(header[9:].split('\t'))
    for line in vcf:
        line = line.rstrip().split('\t')
        
        #Only retain polymorphic SNP lines after filtering
        ref, het, alt, missing, allele_count, total = 0, 0, 0, 0, 0, 0
        RAFs = list()
        
        for i, genotype in enumerate(line[9:]):
            genotype = genotype.split('/')
            if all(x == '0' for x in genotype):
                ref += 1
            elif all(x == '.' for x in genotype):
                missing += 1
            elif all(x == '1' for x in genotype):
                alt += 1
            else:
                het += 1
                
            if str(i) not in populations:
                allele_count += genotype.count('0')
                total += (genotype.count('0') + genotype.count('1'))
            else:
                if total != 0:
                    RAF = allele_count / total
                else:
                    RAF = 0
                RAFs.append(RAF)
                allele_count = genotype.count('0')
                total = (genotype.count('0') + genotype.count('1'))
                
        if total != 0:
            RAF = allele_count / total
        else:
            RAF = 0
        RAFs.append(RAF)
        
        if ref + missing < len(line[9:]) and alt + missing < len(line[9:]) and het / (het + ref + alt) < max_prop_het and any(min_freq <= x <= 1 - min_freq for x in RAFs) and (missing / (missing + ref + alt + het)) <= (1 - c):
            print('\t'.join(e for e in line), file = vcf_new)
    return


def Heterozygosity(vcf = args['VCF_file'], out_dir = args['output_directory'], max_prop_het = args['maximum_proportion_heterozygous'], min_freq = args['minimum_frequency'], plot = args['plot'], plot_type = args['plot_type'], c = args['completeness']):
    '''
    Calculate the mean expected heterozygosity for each sample.
    Two output files are created: 
    *_mh*_mf*_c*_meanHe.txt (a table with all mean expected heterozygosity values)
    Mean_expected_Heterozygosity_*_mh*_mf*_c*.pdf/.png (a bar plot with mean expected heterozygosity values).
    '''
    #Extract the name of the input file.
    name_vcf = os.path.splitext(vcf)[0]
    
    #Open the input file and create a new output file.
    file = open('{}/{}_mh{}_mf{}_c{}.vcf'.format(out_dir, name_vcf, max_prop_het, min_freq, c))
    new_file = open('{}/{}_mh{}_mf{}_c{}_meanHe.txt'.format(out_dir, name_vcf, max_prop_het, min_freq, c), 'w+')
    
    #Extract the header from the input file.
    header = file.readline().rstrip().split('\t')[9:]
    
    #Create a dictionary with the sample names as keys and the expected heterozygosity as value.
    he_dict = defaultdict(dict)
    for sample in header:
        he_dict[sample]['He'] = list()
        
    #Iterate over the SNP lines.
    for line in file:
        line = line.rstrip().split('\t')
        
        #Iterate over the genotypes.
        for i, gt in enumerate(line[9:]):
            gt = gt.split('/')
            if '.' not in gt:
                he_dict[header[i]]['He'].append(2 * (gt.count('0') / len(gt)) * (gt.count('1') / len(gt))) 
                
    #Calculate the mean He and print it to the output file.
    means = list()
    for sample in sorted(he_dict.keys()):
        mean_he = np.mean(he_dict[sample]['He'])
        means.append(mean_he)
        print('{}\t{}'.format(sample, mean_he), file = new_file)
        
    if plot:
        sys.stderr.write('* Plotting the mean expected heterozygosity per population in a barplot ...\n')
        #Create a barplot of the mean expected heterozygosity.
        r = list(np.arange(1, len(means) + 1))
        plt.figure(figsize=(10,10))
        plt.bar(r, means, color = 'g')
        plt.title('Mean expected heterozygosity per sample in\n{}_mh{}_c{}.vcf'.format(name_vcf, max_prop_het, c))
        plt.xlabel('Population')
        plt.ylabel('Mean expected heterozygosity')
        plt.xticks(np.arange(0,len(means) + 1,5))
        if plot_type == 'pdf':
            plt.savefig('{}/Mean_expected_Heterozygosity_{}_mh{}_mf{}_c{}.pdf'.format(out_dir, name_vcf, max_prop_het, min_freq, c), format = 'pdf')
        else:
            plt.savefig('{}/Mean_expected_Heterozygosity_{}_mh{}_mf{}_c{}.pdf'.format(out_dir, name_vcf, max_prop_het, min_freq, c), format = 'png')
        plt.close()
    return


#===============================================================================
# Script
#===============================================================================

if __name__ == '__main__':

    print_date()
    sys.stderr.write("* Filtering {} ...\n".format(args['VCF_file']))
    filterVCF()
    
    if args['He']:
        sys.stderr.write("* Calculate mean expected heterozygosity for each sample ...\n")
        Heterozygosity()
        
    sys.stderr.write("* Finished! \n\n")
    print_date()
