#!usr/bin/python3

#===============================================================================
# FilterReproducibleVariants_Pools
#===============================================================================
#Script to select reproducible variants among technical replicates in an allele frequency table file (pool-seq).
#Yves BAWIN September 2018

#===============================================================================
# Import modules
#===============================================================================

import os, argparse, sys, math
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt

#===============================================================================
# Parse arguments
#===============================================================================

#Create an ArgumentParser object
parser = argparse.ArgumentParser(description = 'Select reproducible variants among technical replicates in a variant frequency table (pool-seq, biallellic variants).')

#Mandatory argument
'''
Argument that include the path to the input file.
'''

parser.add_argument('-f', '--file',
                    type = str,
                    help = 'Input variant frequency table.')

#Optional arguments
'''
Input data options.
'''
parser.add_argument('-i', '--input_directory',
                    type = str,
                    default = '.',
                    help = 'Input directory (default = current directory).')
parser.add_argument('-n', '--number_of_replicates',
                    type = int,
                    default = 2,
                    help = 'Number of replicates of each pool in the variant frequency table (default = 2).')

'''
Analysis options.
'''
parser.add_argument('--mean_frequency',
                    dest = 'mean_frequency',
                    action = 'store_true',
                    help = 'Calculate the mean variant frequency for each pool based on the frequencies of technical replicates (default = mean variant frequencies not calculated).')

'''
Output data options.
'''
parser.add_argument('-o', '--output_directory',
                    type = str,
                    default = '.',
                    help = 'Output directory (default = current directory).')
parser.add_argument('--plot',
                    dest = 'plot',
                    action = 'store_true',
                    help = 'Plot variant frequency distributions (default = no plots).')
parser.add_argument('--plot_type',
                    choices = ['pdf', 'png'], 
                    default = 'pdf',
                    help = 'File format of plots (default = pdf).')

#Parse arguments to a dictionary
args = vars(parser.parse_args())

#===============================================================================
# Functions
#===============================================================================
def print_date ():
    '''
    Print the current date and time to stderr.
    '''
    sys.stderr.write('-------------------\n')
    sys.stderr.write('{}\n'.format(datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
    sys.stderr.write('-------------------\n\n')
    return


def ReproducibleFilter(file = args['file'], in_dir = args['input_directory'], out_dir = args['output_directory'], n = args['number_of_replicates']):
    '''
    Exclude all non-reproducible variants in a pool.
    Two output file is created: *_reproducible_variants.txt (a table with only reproducible frequencies).
                                *_significant_frequencies.txt (a table with frequencies that are significant in both replicates of each pool).
    '''
    #Extract the file name from the original variant frequency table.
    name = os.path.splitext(file)[0]
    
    #Open the variant frequency table.
    file = open('{}/{}'.format(in_dir, file))
    
    #Create new output files.
    new_file = open('{}/{}_reproducible_variants.txt'.format(out_dir, name),'w+')
    signif_freq = open('{}/{}_significant_frequencies.txt'.format(out_dir, name),'w+')
    
    #Extract the header from the input file.
    header = file.readline()
    header = header.rstrip().split('\t')
    
    #Print the header to the output files.
    print('\t'.join(name for name in header), file = new_file)
    print('\t'.join(name for name in header), file = signif_freq)
    
    #Iterate over all variant lines and evaluate each pair of data (corresponding to the replicates).
    for line in file:
        line = line.rstrip().split('\t')
        
        #Create a list for the corrected variant frequencies.
        freq_corrected = list()
        
        #Evaluate each pair of frequencies.
        for i in range(2, len(line), n):
            #Erase the value in variant line if only one of the replicates has data.
            if i + n > len(line):
                freq_corrected.extend(('',) * n)
            elif any(line[x] == '' for x in range(i, i + n)):
                freq_corrected.extend(('',) * n)
            #Convert the value that differs from 0 to 0 if any replicate has a frequency of 0.
            elif any(float(line[x]) == 0.0 for x in range(i, i + n)):
                freq_corrected.extend(('0.0',) * n)
            #Convert the value that differs from 1 to 1 if any replicate has a frequency of 1.
            elif any(float(line[x]) == 1.0 for x in range(i, i + n)):
                freq_corrected.extend(('1.0',) * n)
            #Copy the variant frequencies if the frequencies in all replicates significantly differ from 0 and 1.
            else:
                freq_corrected.extend((line[x] for x in range(i, i + n)))
                
        #Create a list with the frequencies in freq_corrected with only frequencies that are significant in both replicates of a pool.
        signif_frequencies = [x if x != '0.0' and x != '1.0' else '' for x in freq_corrected]
        
        #Check whether at least one pool has a variant frequency that significantly differs from 0 and 1.
        freq_corrected_wo_NA = list(filter(None, freq_corrected))
        if not sum([float(freq) for freq in freq_corrected_wo_NA]) == 0 and not sum([float(freq) for freq in freq_corrected_wo_NA]) == len(freq_corrected_wo_NA):
            #Print the corrected values to the output file together with the contig/scaffold number and the position of the variant.
            print('{}\t{}\t'.format(line[0], line[1]), end = '', file = new_file)
            print('\t'.join(freq for freq in freq_corrected), file = new_file)
            
            print('{}\t{}\t'.format(line[0], line[1]), end = '', file = signif_freq)
            print('\t'.join(sign_freq for sign_freq in signif_frequencies), file = signif_freq)
    return


def MeanAlleleFreq(file = args['file'], in_dir = args['input_directory'], out_dir = args['output_directory'], n = args['number_of_replicates']):
    '''
    Calculate the mean allele frequency from a frequency estimates of two replicates.
    Two output files are created: *_mean_variant_freq.txt (a table with the mean variant frequencies)
                                  *_std_mean_variant_freq.txt (a table with the standard deviation of the mean variant frequencies)
    '''
    #Extract the file name from the input frequency table.
    name = os.path.splitext(file)[0]
    
    #Open the variant frequency table.
    file = open('{}/{}_reproducible_variants.txt'.format(out_dir, name))
    
    #Create a new file for the mean allele frequencies and one for the standard deviations.
    new_file = open('{}/{}_reproducible_variants_mean_variant_freq.txt'.format(out_dir, name),'w+')
    new_file_std = open('{}/{}_reproducible_variants_std_variant_allele_freq.txt'.format(out_dir, name),'w+')
    
    #Extract the header from the input file.
    header = file.readline()
    header = header.rstrip().split('\t')
    
    #Print the header to the output files (the name of replicate 1 is taken as column header in the output file).
    print('{}\t{}\t'.format(header[0], header[1]), end = '', file = new_file)
    print('\t'.join(header[i] for i in range(2, len(header), 2)), file = new_file)
    
    print('{}\t{}\t'.format(header[0], header[1]), end = '', file = new_file_std)
    print('\t'.join(header[i] for i in range(2, len(header), 2)), file = new_file_std)
    
    #Iterate over all SNP lines in the frequency table.
    for line in file:
        line = line.rstrip().split('\t')
        
        #Print the contig name and the position to the output file.
        print('{}\t{}\t'.format(line[0], line[1]), end = '', file = new_file)
        print('{}\t{}\t'.format(line[0], line[1]), end = '', file = new_file_std)
        
        #Create a list for all mean variant frequencies and for all standard deviations and iterate over all frequencies in the variant line.
        MAF_list = list()
        std_list = list()
        for i in range(2, len(line), n):
            if i + n > len(line):
                MAF, std = '', ''
            elif any(line[x] == '' for x in range(i, i + n)):
                MAF, std = '', ''
            else:
                #Calculate the mean variant frequency (MAF) if data are available.
                MAF = sum((float(line[x]) for x in range(i, i + n))) / n
                std = math.sqrt(sum((float(line[x]) - float(MAF))**2 for x in range(i, i + n))/(n - 1))
                
            #Append MAF to a list with all MAFs.
            MAF_list.append(MAF)
            std_list.append(std)
            
        #Print the MAFs and stds of one SNP line to the output file.
        print('\t'.join(str(MAF) for MAF in MAF_list), file = new_file)
        print('\t'.join(str(std) for std in std_list), file = new_file_std)
    return


def histogram (file = args['file'], in_dir = args['input_directory'], out_dir = args['output_directory'], mean_freq = args['mean_frequency'], plot_type = args['plot_type']):
    '''
    General function to create a histogram for the given values.
    '''
    
    #Extract the file name.
    name = os.path.splitext(file)[0]
    
    #Open the input file.
    file = open('{}/{}_reproducible_variants{}.txt'.format(out_dir, name, '_mean_variant_freq' if mean_freq else ''))
    values = dict()
    header = file.readline().rstrip().split('\t')[2:]
    for name in header:
        values[name] = list()
    
    #Create a histogram.
    for line in file:
        line = line.rstrip().split('\t')[2:]
        for i, value in enumerate(line):
            if value != '' and 0 < float(value) < 1:
                values[header[i]].append(float(value))
    
    for name, value in values.items():
        plt.figure(figsize=(10,10))
        plt.hist(value, alpha = 0.7, color = 'darkslategray', bins=(np.linspace(0,1,101)))
        plt.title('{}_reproducible_variants{}'.format(name, 'mean_variant_freq' if mean_freq else ''))
        plt.xlabel('Variant frequency')
        plt.ylabel('Frequency')
        
        if plot_type == 'pdf':
            plt.savefig('{}/Histogram_{}_reproducible_variants{}.pdf'.format(out_dir, name, '_mean_freq' if mean_freq else ''), format='pdf')
        else :
            plt.savefig('{}/Histogram_{}_reproducible_variants{}.png'.format(out_dir, name, '_mean_freq' if mean_freq else ''), format='png')
        
        plt.close()
    return


#===============================================================================
# Script
#===============================================================================

if __name__ == '__main__':

    print_date()
    
    sys.stderr.write('* Filtering to select variants with reproducible frequencies ...\n')
    ReproducibleFilter()
    
    if args['mean_frequency']:
        sys.stderr.write('* Calculating the mean variant frequencies and standard deviations ...\n')
        MeanAlleleFreq()
    
    if args['plot']:
        sys.stderr.write('* Plotting the variant frequencies of each sample in a histogram ...\n')
        histogram()
    
    sys.stderr.write('* Finished!\n')
    
    print_date()
    
