#!usr/bin/python3

#===============================================================================
# FilterVCF_Individuals
#===============================================================================

#Yves BAWIN November 2018
#Script to filter a VCF file.

#===============================================================================
# Import modules
#===============================================================================

import os, sys, argparse
from datetime import datetime

#===============================================================================
# Parse arguments
#===============================================================================

#Create an ArgumentParser object
parser = argparse.ArgumentParser(description = 'Exclude sites in a VCF file with a low allele count, mean depth, and/or quality score, and genotypes with a low depth and/or quality score. Non-polymorphic sites and non-biallelic sites can additionaly be removed.')

#Mandatory arguments

'''
Argument that defines the path to the vcf file.
'''

parser.add_argument('--vcf',
                    type = str,
                    help = 'Path to the vcf file.')

#Optional arguments

'''
Input data options.
'''
parser.add_argument('--ploidy',
                    type = int,
                    default = 2,
                    help = 'Ploidy level of genotype calls in the VCF file (default = 2).')

'''
Filter options for site characteristics.
'''
parser.add_argument('--min_meanDP',
                    type = int,
                    default = 0,
                    help = 'Minimum depth of a site averaged over all samples (default = 0).')
parser.add_argument('--minMAC',
                    type = int,
                    default = 0,
                    help = 'Minimum minor allele count of a site (default = 0).')
parser.add_argument('--minQ',
                    type = float,
                    default = 0,
                    help = 'Minimum quality of a site (default = 0).')
parser.add_argument('--polymorphic',
                    dest = 'polymorph',
                    action = 'store_true',
                    help = 'Remove non-polymorphic sites (default = non-polymorphic sites not removed).')
parser.add_argument('--biallelic',
                    dest = 'biall',
                    action = 'store_true',
                    help = 'Use this option to remove non-biallelic sites (default = non-biallelic sites not removed.')

'''
Filter options for genotype characteristics.
'''
parser.add_argument('--minGQ',
                    type = float,
                    default = 0,
                    help = 'Minimum quality score of a genotype (default = 0).')
parser.add_argument('--minDP',
                    type = int,
                    default = 0,
                    help = 'Minimum depth of a genotype (default = 0).')

#Parse arguments to a dictionary
args = vars(parser.parse_args())

#===============================================================================
# Functions
#===============================================================================
def print_date ():
    '''
    Print the current date and time to stderr.
    '''
    sys.stderr.write('-------------------\n')
    sys.stderr.write('{}\n'.format(datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
    sys.stderr.write('-------------------\n\n')
    return


def Filter_sites(vcf = args['vcf'], min_meanDP = args['min_meanDP'], min_mac = args['minMAC'], minQ = args['minQ']):
    '''
    Filter a VCF file for site characteristics (minimum mean depth, minimum minor allele count, and minimum quality).
    One output file is created: *_(DP)_(MAC)_(q).vcf
    '''
    #Extract the basename from the VCF file.
    name = os.path.splitext(vcf)[0]
    
    #Open the VCF file.
    vcf = open('{}.vcf'.format(name))
    
    #Create the output file.
    out_file = open('{}{}{}{}.vcf'.format(name, '_DP{}'.format(min_meanDP) if min_meanDP > 0 else '', '_MAC{}'.format(min_mac) if min_mac > 0 else '', '_q{}'.format(minQ) if minQ > 0 else ''), 'w+')
    
    #Filter for a minimal average depth, a minimal minor allele count and a minimal site quality.
    for line in vcf:
        #Print the header to the output file.
        if line.startswith('#CHROM'):
            print(line, end = '', file = out_file)
        elif not line.startswith('##'):
            SNP = line.rstrip().split('\t')
            Q = float(SNP[5])
            #Filter for site quality.
            if Q >= minQ:
                #Filter for minor allele count.
                info = SNP[7].split(';')
                ALT = SNP[4].split(',')
                AC = info[0][3:].split(',')
                AC_new = 'AC='
                ALT_new = list()
                for index, count in enumerate(AC):
                    if int(count) >= min_mac:
                        if len(AC_new) > 3:
                            AC_new += ',{}'.format(count)
                        else:
                            AC_new += str(count)
                        ALT_new.append(ALT[index])
                info[0] = AC_new
                SNP[7] = ';'.join(x for x in info)
                SNP[4] = ','.join(x for x in ALT_new)
                if len(ALT_new) > 0:
                    ref_count = int(info[2][3:])
                    for x in AC_new[3:].split(','):
                        ref_count -= int(x)
                    if ref_count >= min_mac:
                        #Filter for mean depth.
                        genotypes = SNP[9:]
                        depth = 0
                        NumberOfSamples = 0
                        for genotype in genotypes:
                            genotype = genotype.split(':')
                            if len(genotype) > 1:
                                depth += int(genotype[2])
                                NumberOfSamples += 1
                        avg_depth = depth / NumberOfSamples
                        
                        #Print the variants that passed all site filters.
                        if avg_depth >= min_meanDP:
                            print('\t'.join(e for e in SNP), file = out_file)
    return


def Filter_genotypes(vcf = args['vcf'], min_meanDP = args['min_meanDP'], min_mac = args['minMAC'], minQ = args['minQ'], minGQ = args['minGQ'], min_DP = args['minDP'], ploidy = args['ploidy']):
    '''
    Filter a VCF file for genotype characteristics (minimum genotype quality, minimum genotype depth).
    One output file is created: *_(GQ)_(DP).vcf
    '''
    
    #Extract the basename from the VCF file.
    name = os.path.splitext(vcf)[0]
    
    #Open the polyploid VCF file filtered for subgenome differences.
    vcf = open('{}{}{}{}.vcf'.format(name, '_DP{}'.format(min_meanDP) if min_meanDP > 0 else '', '_MAC{}'.format(min_mac) if min_mac > 0 else '', '_q{}'.format(minQ) if minQ > 0 else ''))
    
    #Create the output file.
    out_file = open('{}{}{}{}{}{}.vcf'.format(name, '_DP{}'.format(min_meanDP) if min_meanDP > 0 else '', '_MAC{}'.format(min_mac) if min_mac > 0 else '', '_q{}'.format(minQ) if minQ > 0 else '', '_GQ{}'.format(minGQ) if minGQ > 0 else '', '_DP{}'.format(min_DP) if min_DP > 0 else ''), 'w+')
    
    #Filter for a minimal genotype quality and a minimal genotype depth.
    for line in vcf:
        #Print the header to the output file and count the number of samples in the VCF file.
        if line.startswith('#CHROM'):
            print(line, end = '', file = out_file)
            NumberOfSamples = len(line.rstrip().split('\t')[9:])
        elif not line.startswith('##'):
            line = line.rstrip().split('\t')
            i = 9
            missing = 0
            for genotype in line[9:]:
                #Check whether the genotype quality and depth is higher than the minimum. If one of them is lower, convert the genotype call into a missing value.
                genotype = genotype.split(':')
                if genotype[0] != '.' + '/.' * (ploidy - 1):
                    depth, quality = int(genotype[2]), float(genotype[3])
                    if depth < min_DP or quality < minGQ:
                        line[i] = '.' + ('/.' * (ploidy - 1)) + ':' + ':'.join(value for value in genotype[1:])
                        line[i] = line[i].rstrip()
                        missing += 1
                else:
                    missing += 1
                i += 1
            if missing < NumberOfSamples:
                print('\t'.join(value for value in line), file = out_file)
    return


def Filter_PolymorphicSites(vcf = args['vcf'], min_meanDP = args['min_meanDP'], min_mac = args['minMAC'], minQ = args['minQ'], minGQ = args['minGQ'], min_DP = args['minDP'], ploidy = args['ploidy']):
    '''
    Filter a VCF file for polymorphic sites.
    One output file is created: *_polymorphic.vcf
    '''
    #Extract the basename from the VCF file.
    name = os.path.splitext(vcf)[0]
    
    #Open the (filtered) VCF file.
    vcf = open('{}{}{}{}{}{}.vcf'.format(name, '_DP{}'.format(min_meanDP) if min_meanDP > 0 else '', '_MAC{}'.format(min_mac) if min_mac > 0 else '', '_q{}'.format(minQ) if minQ > 0 else '', '_GQ{}'.format(minGQ) if minGQ > 0 else '', '_DP{}'.format(min_DP) if min_DP > 0 else ''))
    
    #Create the output file.
    out_file = open('{}{}{}{}{}{}_polymorphic.vcf'.format(name, '_DP{}'.format(min_meanDP) if min_meanDP > 0 else '', '_MAC{}'.format(min_mac) if min_mac > 0 else '', '_q{}'.format(minQ) if minQ > 0 else '', '_GQ{}'.format(minGQ) if minGQ > 0 else '', '_DP{}'.format(min_DP) if min_DP > 0 else ''), 'w+')
    
    #filter for polymorphic sites.
    for line in vcf:
        #Print the header to the output file and count the number of samples in the VCF file.
        if line.startswith('#CHROM'):
            print(line, end = '', file = out_file)
            line = line.rstrip().split('\t')[9:]
            NumberOfSamples = len(line)
        elif not line.startswith('##'):
            genotype_calls = line.rstrip().split('\t')[9:]
            missing, hom_ref, hom_alt, het = 0, 0, 0, 0
            #Iterate over the genotype calls.
            for genotype in genotype_calls:
                genotype = genotype.split(':')
                if genotype[0] == '.' + '/.' * (ploidy - 1):
                    missing += 1
                elif genotype[0] == '0' + '/0' * (ploidy - 1):
                    hom_ref += 1
                elif genotype[0] == '1' + '/1' * (ploidy - 1):
                    hom_alt += 1
                else:
                    het += 1
            if missing + hom_ref < NumberOfSamples and missing + hom_alt < NumberOfSamples and missing + het < NumberOfSamples:
                print(line, end = '', file = out_file)
    return


def Filter_BiallelicSites(vcf = args['vcf'], min_meanDP = args['min_meanDP'], min_mac = args['minMAC'], minQ = args['minQ'], minGQ = args['minGQ'], min_DP = args['minDP'], ploidy = args['ploidy'], polymorph = args['polymorph']):
    '''
    Filter a VCF file for biallelic sites.
    One output file is created: *_biallelic.vcf
    '''
    #Extract the basename from the VCF file
    name = os.path.splitext(vcf)[0]
    
    #Open the (filtered) VCF file.
    vcf = open('{}{}{}{}{}{}{}.vcf'.format(name, '_DP{}'.format(min_meanDP) if min_meanDP > 0 else '', '_MAC{}'.format(min_mac) if min_mac > 0 else '', '_q{}'.format(minQ) if minQ > 0 else '', '_GQ{}'.format(minGQ) if minGQ > 0 else '', '_DP{}'.format(min_DP) if min_DP > 0 else '', '_polymorphic' if polymorph else ''))
    
    #Create the output file.
    out_file = open('{}{}{}{}{}{}{}_biallelic.vcf'.format(name, '_DP{}'.format(min_meanDP) if min_meanDP > 0 else '', '_MAC{}'.format(min_mac) if min_mac > 0 else '', '_q{}'.format(minQ) if minQ > 0 else '', '_GQ{}'.format(minGQ) if minGQ > 0 else '', '_DP{}'.format(min_DP) if min_DP > 0 else '', '_polymorphic' if polymorph else ''), 'w+')
    
    #Filter for biallelic sites.
    for line in vcf:
        #Print the header to the output file.
        if line.startswith('#CHROM'):
            print(line, end = '', file = out_file)
        elif not line.startswith('##'):
            line = line.rstrip().split('\t')
            #Count the number of alleles in each variant position. Keep the variant if there are just two alleles.
            AlleleCount = len(line[4].split(',')) + 1
            if AlleleCount == 2:
                print('\t'.join(value for value in line), file = out_file)
    return


#===============================================================================
# Script
#===============================================================================

if __name__ == '__main__':

    print_date()
    if args['min_meanDP'] > 0 or args['minMAC'] > 0 or args['minQ']> 0 :
        sys.stderr.write("* Removing variants with a mean depth lower than {}, a minor allele count lower than {}, and a quality score lower than {} ... \n".format(args['min_meanDP'], args['minMAC'], args['minQ']))
        Filter_sites()
    if args['minGQ'] > 0 or args['minDP'] > 0 :
        sys.stderr.write("* Removing genotypes with a depth lower than {} and a quality lower than {} ... \n".format(args['minDP'], args['minGQ']))
        Filter_genotypes()
    if args['polymorph']:
        sys.stderr.write("* Removing non-polymorphic sites ... \n")
        Filter_PolymorphicSites()
    if args['biall']:
        sys.stderr.write("* Removing non-biallelic sites ... \n")
        Filter_BiallelicSites()
    sys.stderr.write("* Finished! \n\n")
    print_date()
    
